package EmailAdmin;

import java.util.Scanner;

public class Email {
	private String firstName;
	private String lastName;
	private String password;
	private String department;
	private int mailCapacity;
	private String alternativeEmail;

	// create a constructor to pass the First Name and Last Name

	public Email(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		// System.out.println("Email created for :" + firstName + " " + lastName);
		this.department = getDepartment();// Whatever the Method value is returned is passed to Department.
		// System.out.println("First Name:" + firstName + " \nLast Name:" + lastName +
		// "\nDept:" + this.department);

	}

	// Get the Department from the user
	private String getDepartment() {
		System.out.println(firstName + ", Can you please enter the Department code\n 1.IT \n 2.Sales \n 3.Finance");
		Scanner in = new Scanner(System.in);
		int capture = in.nextInt();
		if (capture == 1) {
			return "IT";
		} else if (capture == 2) {
			return "Sales";
		} else if (capture == 3) {
			return "Finance";
		} else
			return "You have entered Invalid department Number";

	}

	public String setEmail(String email) {
		this.alternativeEmail = email;
		return email;
	}

	public int setCapcaticy(int cap) {
		this.mailCapacity = cap;
		return cap;
	}

	public String getInfo() {
		return "Full Name: " + firstName + " " + lastName + "\nDepartment: " + department + "\nEmail: "
				+ alternativeEmail;
	}

}
